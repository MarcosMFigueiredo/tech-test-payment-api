using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public Produto Produtos { get; set; }

    }
}