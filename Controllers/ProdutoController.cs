using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    public class ProdutoController : ControllerBase
    {
        private readonly VendaContext _contex;

        public ProdutoController(VendaContext context)
        {
            _contex = context;
        }

        [HttpPost("CadastrarProduto")]
        public IActionResult CadastrarProduto(Produto produto)
        {
            _contex.Add(produto);
            _contex.SaveChanges();

            return Ok(produto);
        }
    }
}