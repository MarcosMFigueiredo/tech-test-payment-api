using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _contex;

        public VendedorController(VendaContext context)
        {
            _contex = context;
        }

        [HttpPost("RegistrarVendedor")]
        public IActionResult RegistrarVendedor(Vendedor vendedor)
        {
            _contex.Add(vendedor);
            _contex.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("BuscarVendedorPor{id}")]
        public IActionResult BuscarVendedorPorId(int id)
        {
            var vendedor = _contex.Vendedores.Find(id);

            if (vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
    }
}