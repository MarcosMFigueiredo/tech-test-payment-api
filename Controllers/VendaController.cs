using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext venda)
        {
            _context = venda;
        }

        [HttpGet("BuscarVenda")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistarVenda(int IdVendedor, int IdProduto, Venda venda)
        {
            var vendedor = _context.Vendedores.Find(IdVendedor);
            var produtos = _context.Produtos.Find(IdProduto);

            if (vendedor is null)
                return NotFound("Vendedor não encontrado!!!");

            if (produtos == null)
                return BadRequest("Venda deve possuir pelo menos 1 item!");

            venda.Status = EnumStatusVenda.AguardandoPagamento;
            venda.Vendedor = vendedor;
            venda.Produtos = produtos;
            venda.Data = DateTime.Now;


            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("AtualizarVenda")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco.Status == EnumStatusVenda.AguardandoPagamento)
            {
                if (venda.Status == EnumStatusVenda.PagamentoAprovado)
                {
                    vendaBanco.Status = venda.Status;
                }
                else if (venda.Status == EnumStatusVenda.Cancelado)
                {
                    vendaBanco.Status = venda.Status;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização inválida" });
                }
            }

            else if (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado)
            {
                if (venda.Status == EnumStatusVenda.EnviadoParaTransportadora)
                {
                    vendaBanco.Status = venda.Status;
                }
                else if (venda.Status == EnumStatusVenda.Cancelado)
                {
                    vendaBanco.Status = venda.Status;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização inválida" });
                }
            }

            else if (vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora)
            {
                if (venda.Status == EnumStatusVenda.Entregue)
                {
                    vendaBanco.Status = venda.Status;
                }
                else
                {
                    return BadRequest(new { Erro = "Atualização inválida" });
                }
            }

            else
            {
                return BadRequest(new { Erro = "Não é possível realizar esta alteração" });
            }

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);


        }
    }

}
